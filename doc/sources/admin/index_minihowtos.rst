Mini Howtos
===========

.. toctree::
   :maxdepth: 1

   cli_examples
   managerprotection
   mysqlminihowto
   ldapminihowto
   restminihowto
   soapminihowto
   activedirectoryminihowto
   kerberos
   federationproxy
   header_remote_user_conversion
   renater
   samlfederation
   behindproxyminihowto
   useoutgoingproxy
   testopenidconnect
   anssi-oidc
   oidc-agent-connect
